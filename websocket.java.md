## Jetty Websocket Server

[Websocket](https://github.com/jetty-project/embedded-jetty-websocket-examples)

[Simple example](https://jansipke.nl/websocket-tutorial-with-java-server-jetty-and-javascript-client/)

```
import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.server.ServerEndpoint;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.eclipse.jetty.websocket.server.WebSocketHandler;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class WebSocketTest {
	@WebSocket
	@ServerEndpoint(value = "/events/")
	public static class MyWebSocketHandler {

		@OnWebSocketClose
		public void onClose(int statusCode, String reason) {
			System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
		}

		@OnWebSocketError
		public void onError(Throwable t) {
			System.out.println("Error: " + t.getMessage());
		}

		@OnWebSocketConnect
		public void onConnect(Session session) {
			System.out.println("Connect: " + session.getRemoteAddress().getAddress());
			try {
				session.getRemote().sendString("Hello Webbrowser");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		@OnWebSocketMessage
		public void onMessage(String message) {
			System.out.println("Message: " + message);
		}
	}

	private static class MyWebSocketServer implements Runnable {

		@Override
		public void run() {
			Server server = new Server(8088);

			WebSocketHandler wsHandler = new WebSocketHandler() {
				@Override
				public void configure(WebSocketServletFactory factory) {
					factory.register(MyWebSocketHandler.class);
				}
			};

			server.setHandler(wsHandler);

			try {
				System.out.println("=== websocket server served at 8088");
				server.start();
				server.join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	private static class MyHttpServer implements Runnable {

		public static class HelloServlet extends HttpServlet {

			/**
			 * 
			 */
			private static final long serialVersionUID = -270441462823449739L;

			protected void doGet(javax.servlet.http.HttpServletRequest req, javax.servlet.http.HttpServletResponse resp)
					throws javax.servlet.ServletException, java.io.IOException {
				System.out.println("=== do get detected - " + req.getServletPath() + "/" + req.getProtocol());

				if (req.getServletPath().equalsIgnoreCase("/")) {
					resp.setContentType("text/html; charset=UTF-8");
					resp.setStatus(HttpServletResponse.SC_OK);
					resp.getWriter().write("<!DOCTYPE html>\r\n" + 
							"<html>\r\n" + 
							"    <body>\r\n" + 
							"        <h1>WebSocket test</h1>\r\n" + 
							"        <script>\r\n" + 
							"        \r\n" + 
							"        var ws = new WebSocket(\"ws://127.0.0.1:8088/events/\");\r\n" + 
							"		\r\n" + 
							"		ws.onopen = function() {\r\n" + 
							"		    alert(\"Opened!\");\r\n" + 
							"		    ws.send(\"Hello Server\");\r\n" + 
							"		};\r\n" + 
							"		\r\n" + 
							"		ws.onmessage = function (evt) {\r\n" + 
							"		    alert(\"Message: \" + evt.data);\r\n" + 
							"		};\r\n" + 
							"		\r\n" + 
							"		ws.onclose = function() {\r\n" + 
							"		    alert(\"Closed!\");\r\n" + 
							"		};\r\n" + 
							"		\r\n" + 
							"		ws.onerror = function(err) {\r\n" + 
							"		    alert(\"Error: \" + err);\r\n" + 
							"		};\r\n" + 
							"		\r\n" + 
							"		setInterval(() => {\r\n" + 
							"			ws.send(\"Hello World\");\r\n" + 
							"		}, 1000);\r\n" + 
							"        </script>\r\n" + 
							"    </body>\r\n" + 
							"</html>");
				}
				resp.getWriter().close();
			}
		}

		@Override
		public void run() {
			Server server = new Server();
			ServerConnector connector = new ServerConnector(server);
			connector.setPort(8084);
			server.addConnector(connector);

			ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			context.setContextPath("/");
			server.setHandler(context);

			ServletHolder holderPwd = new ServletHolder("default", HelloServlet.class);
			holderPwd.setInitParameter("dirAllowed", "false");
			context.addServlet(holderPwd, "/");

			System.out.println("=== http server served at 8084");

			try {
				System.out.println("=== websocket server served at 8088");
				server.start();
				server.join();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

	public static void main(String[] args) throws Exception {
		new Thread(new MyWebSocketServer()).start();
		new Thread(new MyHttpServer()).start();
	}
}


```
