# mobile-cookie

```plantuml
@startuml

    skinparam handwritten true

    package Host {
        rectangle Files {
        }

        node Builder {
            node PreProcessor 

            node Compiler

            node Linker
        }

        node Packaging {
        }

        Files --> PreProcessor: create compilable sources
        PreProcessor -> Compiler: do compiling
        Compiler -> Linker: link all object files

        Linker -> Packaging: create executable file
    }

    package Guest {

        node Execute {
        }

        Packaging --> Execute: deployment
    }
@enduml
```

Collection of codesnipets


