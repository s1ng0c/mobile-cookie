## Modify `AppCache`, adding icon to `AppInfo`

```java
try {
    app.icon = context.getPackageManager().getApplicationIcon(app.packageName.toString());
} catch (PackageManager.NameNotFoundException e) {
    e.printStackTrace();
}

```