
## Adding fonts

* Create simple TextView
* On **Attributes** setting toolbox, select `fontFamily`
* From `fontFamily` menu, select **More fonts**
* Type font name on search box, wait until the font attibutes displayed on the right panel
* Select **Add font to project**

## Apply font to views programmatically

Change Typeface of view
Ex:
```java
Typeface typeface = mContext.getResources().getFont(R.font.share_tech_mono);
v.setTypeface(typeface);
```
